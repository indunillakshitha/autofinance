<?php

use App\Http\Controllers\BorrowerController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CenterController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\LoanCategoryController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\RepaymentController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    // return Inertia::render('Dashboard');
    return Inertia::render('AdminDashboard');
})->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->get('/profile-dashboard', function () {
    return Inertia::render('Dashboard');
})->name('profile.dashboard');
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {

    Route::prefix('/users')->group(function () { 
        //Users section
        Route::get('/index', [UserController::class, 'index'])->name('users.index');
        Route::get('/create', [UserController::class, 'create'])->name('users.create');
        Route::post('/store', [UserController::class, 'store'])->name('users.store');
        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/update', [UserController::class, 'update'])->name('users.update');
       
    });

    Route::prefix('roles')->group(function () { 
        //Roles section
        Route::get('/index', [RoleController::class, 'index'])->name('roles.index');
        Route::get('/create', [RoleController::class, 'create'])->name('roles.create');
        Route::post('/store', [RoleController::class, 'store'])->name('roles.store');
       
    });
    Route::prefix('branch')->group(function () { 
        //Roles section
        Route::get('/index', [BranchController::class, 'index'])->name('branch.index');
        Route::get('/create', [BranchController::class, 'create'])->name('branch.create');
        Route::post('/store', [BranchController::class, 'store'])->name('branch.store');
       
    });
    Route::prefix('center')->group(function () { 
        //Roles section
        Route::get('/index', [CenterController::class, 'index'])->name('center.index');
        Route::get('/create', [CenterController::class, 'create'])->name('center.create');
        Route::post('/store', [BranchController::class, 'store'])->name('center.store');
       
    });
    Route::prefix('group')->group(function () { 
        //Roles section
        Route::get('/index', [GroupController::class, 'index'])->name('group.index');
        Route::get('/create', [GroupController::class, 'create'])->name('group.create');
        Route::post('/store', [GroupController::class, 'store'])->name('group.store');
       
    });
    Route::prefix('loan-category')->group(function () { 
        //Roles section
        Route::get('/index', [LoanCategoryController::class, 'index'])->name('loan-category.index');
        Route::get('/create', [LoanCategoryController::class, 'create'])->name('loan-category.create');
        Route::post('/store', [LoanCategoryController::class, 'store'])->name('loan-category.store');
       
    });
    Route::prefix('loan')->group(function () { 
        //Roles section
        Route::get('/index', [LoanController::class, 'index'])->name('loan.index');
        Route::get('/create', [LoanController::class, 'create'])->name('loan.create');
        Route::post('/store', [LoanController::class, 'store'])->name('loan.store');
       
    });
    Route::prefix('repayment')->group(function () { 
        //Roles section
        Route::get('/center', [RepaymentController::class, 'center'])->name('repayment.center');
        Route::get('/office', [RepaymentController::class, 'office'])->name('repayment.office');
       
    });
    Route::prefix('borrower')->group(function () { 
        //Roles section
        Route::get('/index', [BorrowerController::class, 'index'])->name('borrower.index');
        Route::get('/create', [BorrowerController::class, 'create'])->name('borrower.create');
        Route::post('/store', [BorrowerController::class, 'store'])->name('borrower.store');
       
    });
});